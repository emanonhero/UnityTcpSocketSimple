# UnityTcpSocketSimple

#### 介绍

unity3D Tcp框架, 基于Telepathy , 包含服务端和客户端, 用于unity内端通讯,高效稳定.

Telepathy 是unity Mirror联机插件里面的应用的

Telepathy https://github.com/vis2k/Telepathy

#### 使用说明
Client:

    public class ClientManager : BaseTcp
    {
        private void Start()
        {
            InvokeRepeating("ConnectedInterval", 0, connectInterval); //发送心跳
        }

        #region 其他消息接收
        protected override void OnConnected(Message msg)
        {
            Debug.LogWarning(string.Format("客户端启动! id:{0}", msg.connectionId));
        }

        protected override void OnDisconnected(Message msg)
        {
            Debug.LogWarning(string.Format("断开链接! id:{0}", msg.connectionId));
        }
        protected override void OnLoginMessage(int connID, Protocols.LoginMessage msg)
        {
            base.OnLoginMessage(connID, msg);
            Dictionary<string, string> keyValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(msg.MsgBase.text);
            foreach (var item in keyValues)
            {
                Debug.LogWarning(string.Format("{0} - {1}", item.Key, item.Value));
            }
        }

        protected override void OnHeardMessage(int connID, Protocols.HeartMessage msg)
        {
            base.OnHeardMessage(connID, msg);
            Debug.LogWarning("收到服务器返回的心跳消息:{0}"+msg.MsgBase.text);
        }
        #endregion

        protected override void OnRecvMessage(int connID, Telepathy.Protocols.MyMessage msg)
        {
            Debug.LogWarning(string.Format("收到来自 {0} 服务端消息:{1}", connID, msg.MsgBase.text));

        }

        /// <summary>
        /// 发送消息
        /// </summary>
        /// <param name="strMsg"></param>
        /// <param name="headerType"></param>
        /// <returns></returns>
        public bool SendMsg(string strMsg, HeaderType headerType = HeaderType.Content, byte[] bytes = null)
        {
            MessageBase myMsg = GetMessageBase(strMsg, bytes, headerType);
            myMsg.messageId = MsgStringId;
            return SendToserver(myMsg.Serialize());
        }

        #region 心跳
        /// <summary>
        /// 设置重连次数
        /// </summary>
        [Space(10)] public int reConnCount = 1000;
        /// <summary>
        /// 链接间隔时间
        /// </summary>
        float connectInterval = 1f;

        /// <summary>
        /// 链接次数
        /// </summary>
        int connectCount = 0;

        public UnityAction<int> OnConnInterval = null;

        /// <summary>
        /// 心跳检查 断开重连 在Update中调用才有效
        /// </summary>
        protected void ConnectedInterval()
        {
            if (client == null || client.Connected == false)
            {
                Destroy();
                if (connectCount < reConnCount)
                {
                    //链接次数增加
                    connectCount++;
                    string str = string.Format("这是第{0}次 连接", connectCount);
                    Debug.Log(str);
                    StartClient(this.Ip, this.Port);  //重连一次
					if(client!=null) client.MaxMessageSize = 1024 * 1024;
                }
                OnConnInterval?.Invoke(connectCount);
            }
            else if (client.Connected)
            {
                SendMsg("1", HeaderType.Heart);
            }
        }
        #endregion
    }


Server:

    public class ServerManager : BaseTcp
    {

        protected override void Awake()
        {
            base.Awake();
            loginManager = new LoginController(this);
        }

        private void Start()
        {
            StartServer(this.Port);
            server.MaxMessageSize = 1024 * 1024;
        }

        protected override void OnConnected(Message msg)
        {
            Debug.LogWarning(string.Format("客户端连接! id:{0}", msg.connectionId));
        }

        protected override void OnDisconnected(Message msg)
        {
            Debug.LogWarning(string.Format("客户端断开! id:{0}", msg.connectionId));
        }

        protected override void OnRecvMessage(int connID, Telepathy.Protocols.MyMessage msg)
        {
            Debug.LogWarning(string.Format("收到来自 {0} 客户端消息, 类型:{1} 内容:{2}", connID, msg.MsgBase.headerType, msg.MsgBase.text));
        }

        protected override void OnLoginMessage(int connID, Protocols.LoginMessage msg)
        {
            base.OnLoginMessage(connID, msg);
			Debug.LogWarning(string.Format("客户端登录操作! id:{0}", msg.connectionId));
        }

        protected override void OnHeardMessage(int connID, Protocols.HeartMessage msg)
        {
            base.OnHeardMessage(connID, msg);
            Debug.LogWarning(string.Format("收到心跳消息:{0}", msg.MsgBase.text));
            SendMsg(connID, msg.MsgBase.text, HeaderType.Heart); //直接返回心跳消息
        }
        /// <summary>
        /// 发送消息
        /// </summary>
        /// <param name="strMsg"></param>
        /// <param name="headerType"></param>
        /// <returns></returns>
        public bool SendMsg(int clientID, string strMsg, HeaderType headerType = HeaderType.Content, byte[] bytes = null)
        {
            MessageBase myMsg = GetMessageBase(strMsg, bytes, headerType);
            myMsg.messageId = MsgStringId;
            return SendToClient(clientID, myMsg.Serialize());
        }

        /// <summary>
        /// 转发使用
        /// </summary>
        /// <param name="clientID"></param>
        /// <param name="myMsg"></param>
        /// <returns></returns>
        public bool RelaySend(int clientID, MessageBase myMsg)
        {
            return SendToClient(clientID, myMsg.Serialize());
        }
    }


#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 码云特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5.  码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
