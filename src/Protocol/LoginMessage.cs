﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

namespace Telepathy.Protocols
{
    public class LoginMessage
    {
        public MessageBase MsgBase;
        public LoginMessage()
        {
            MsgBase = new MessageBase(HeaderType.Login);
        }

    }

}