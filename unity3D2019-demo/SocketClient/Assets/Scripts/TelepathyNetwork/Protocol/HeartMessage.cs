﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

namespace Telepathy.Protocols
{
    public class HeartMessage
    {
        public MessageBase MsgBase;
        public HeartMessage()
        {
            MsgBase = new MessageBase(HeaderType.Content);
        }
    }

}