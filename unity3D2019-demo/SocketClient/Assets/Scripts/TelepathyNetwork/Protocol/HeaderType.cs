﻿public enum HeaderType
{
    /// <summary>
    /// 正文类型消息
    /// </summary>
    Content = 0,
    /// <summary>
    /// 登录类型消息
    /// </summary>
    Login = 1,
    /// <summary>
    /// 心跳类型消息
    /// </summary>
    Heart = 2,
    /// <summary>
    /// 链接消息
    /// </summary>
    Connected=3,
    /// <summary>
    /// 断开消息
    /// </summary>
    Disconnected=4

}